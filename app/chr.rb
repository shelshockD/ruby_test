class ChangeHistoryReport
  def date_range(label1, label2)
    vc = VersionControl.new
    vc.connect
  dates = [label1, label2].collect do |label|
      vc.fetch_label(label).files.sort_by{|f|f['date']}.last['date']
    end
    vc.disconnect
    return dates
  end
end