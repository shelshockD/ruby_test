class VersionControlMaintenance

	DAY_SECONDS = 60 * 60 * 24

	def initialize(vcs)
		@vcs = vcs
	end

	def purge_old_labels(age_in_days)
		@vcs.connect
		old_labels = @vcs.label_list.select do |label|
			label['date'] <= Time.now - age_in_days * DAY_SECONDS
		end
		@vcs.label_delete(*old_labels.collect{|label| label['name']})
		@vcs.disconnect
	end
end