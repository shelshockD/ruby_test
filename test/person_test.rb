# test/person_test.rb
require_relative '../app/person'
require 'test/unit'
# require 'tuxml'

class PersonTest < Test::Unit::TestCase
	def test_first_name
		person = Person.new('Jim', 'Jones', 22)
		assert_equal 'Jim' , person.first_name
	end

	def test_last_name
		person = Person.new('Jim', 'Jones', 22)
		assert_equal 'Jones', person.last_name
	end

	def test_full_name
		person = Person.new('Jim', 'Jones', 22)
		assert_equal 'Jim Jones', person.full_name
	end

	def test_age
		person = Person.new('Jim', 'Jones', 22)
		assert_equal 22, person.age
		assert_raise(ArgumentError) {Person.new('Jim', 'Jones', -7)}
		assert_raise(ArgumentError) {Person.new('Jim', 'Jones', 'two')}
	end
end