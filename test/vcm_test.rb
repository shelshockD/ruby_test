require 'rubygems'
require 'flexmock'
require 'test/unit'
require_relative '../app/vcm'

class VersionControlMaintenanceTest < Test::Unit::TestCase

	DAY_SECONDS = 60 * 60 * 24
	LONG_AGO = Time.now - DAY_SECONDS * 3
	RECENT = Time.now - DAY_SECONDS * 1
	LABEL_LIST = [
					{'name' => 'L1', 'date' => LONG_AGO},
					{'name' => 'L2', 'date' => RECENT}
				]

	def test_purge

		FlexMock.use("vcs") do |vcs|
			vcs.should_receive(:connect).with_no_args.once.ordered
			vcs.should_receive(:label_list).with_no_args.and_return(LABEL_LIST).once.ordered
			vcs.should_receive(:label_delete).with('L1').once.ordered
			vcs.should_receive(:disconnect).with_no_args.once.ordered
			v = VersionControlMaintenance.new(vcs)
			v.purge_old_labels(2)

		# vcs = flexmock("vcs")
		# vcs.should_receive(:connect).with_no_args.once.ordered
		# vcs.should_receive(:label_list).with_no_args.and_return(LABEL_LIST).once.ordered
		# vcs.should_receive(:label_delete).with('L1').once.ordered
		# vcs.should_receive(:disconnect).with_no_args.once.ordered


		# The mock calls will be automatically verified on exiting the @Flexmock.use block
		end
	end
end